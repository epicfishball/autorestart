@echo off
setlocal enabledelayedexpansion
REM -----------------------
REM         ABOUT
REM -----------------------
REM This script was written for Win10 only.

REM Change EXE_PATH and EXE_NAME as needed (they're probably
REM going to be the same).

REM You can change the checking interval too, see below.

REM This script can be started even if the process you want
REM to keep watch on is already running.


REM -----------------------
REM     SCRIPT SETTINGS
REM -----------------------

REM Absolute path the .exe to run
set EXE_PATH=notepad.exe

REM Process name to test for; you can see this in
REM Task Manager
set EXE_NAME=notepad.exe

REM Name of the output logfile; you can use a
REM full absolute path too I think
set LOGFILE=autostart_log
set LOGFILE_FORMAT=txt

REM Time to wait between checking whether the app is running
set UPDATE_INTERVAL_SECS=10

REM Color setting
set COLOUR=0b

REM -----------------------
REM -----------------------


REM Constants
set NOT_RUNNING=0
set IS_RUNNING=1

REM Add timestamp to logfile name
set LOGFILE_FULL=!LOGFILE!_%date:/=-%_%time:~0,2%%time:~3,2%hrs
REM Convert spaces to underscores
set LOGFILE_FULL=!LOGFILE_FULL: =_!
REM Append file format
set LOGFILE_FULL=!LOGFILE_FULL!.%LOGFILE_FORMAT%

REM Variables
set TIMESTAMP=""
set COUNT=0


:init
color %COLOUR%
echo Writing to logfile: !LOGFILE_FULL!
call :writeLog "Auto-restart script started"
call :writeLog "  Process name to check for:  %EXE_NAME%"
call :writeLog "             App to restart:  %EXE_PATH%"


:mainloop
REM Test if app is running
call :isProcRunning %EXE_NAME%

if %errorlevel%==%NOT_RUNNING% (
    REM Append to log
    set /a COUNT=!COUNT!+1
    call :writeLog "Starting %EXE_NAME% (!COUNT!x since start)"
    
    REM Restart app
    start %EXE_PATH%
) else (
    call :snooze
)
goto :mainloop


:isProcRunning
REM Returns ERRORLEVEL 1 if task is running, 0 if not
set foo=0
for /f %%i in ('tasklist^|find "%1"') do (
    set foo=1
)
exit /b !foo!


:updateTimestamp
REM Updates global timestamp var to the current time
set TIMESTAMP=[%date% %time%]
set TIMESTAMP=!TIMESTAMP:/=-!
exit /b


:writeLog
REM Update timestamp var for message
call :updateTimestamp

REM Appends message to log
set arg=%1
set MSG=!TIMESTAMP! %arg:~1,-1%

REM Trim the first and last characters (they are ")
echo !MSG!
echo !MSG!>>!LOGFILE_FULL!
exit /b


:snooze
REM Makes this script sleep
ping /n %UPDATE_INTERVAL_SECS% localhost>nul
exit /b
